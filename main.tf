terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }

  backend "http" {
  }
}

# Configure the AWS Provider
provider "aws" {
  region = "us-east-2" // Ohio
}

resource "aws_s3_bucket" "jar_staging" {
//  https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket#argument-reference
  tags = {
    Environment = "Dev"
  }
}

resource "aws_s3_object" "object" {
  //https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_object#argument-reference
  bucket = aws_s3_bucket.jar_staging.bucket
  key    = "g-hello-0.0.1-SNAPSHOT.jar"
  source = "build/libs/g-hello-0.0.1-SNAPSHOT.jar"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
  source_hash = filemd5("build/libs/g-hello-0.0.1-SNAPSHOT.jar")
}

# resource "aws_instance" "web" {
#   // https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
#   ami           = "ami-02238ac43d6385ab3"
#   instance_type = "t2.micro"
#   key_name = "dennis-tf-demo-ohio"
#   security_groups = [ aws_security_group.allow_SSH.name ]
#   iam_instance_profile = aws_iam_instance_profile.s3_get_object_profile.name

#   user_data = <<EOF
#   #! /bin/bash
#   yum update -y
#   yum install -y java-17-amazon-corretto-headless
#   aws s3api get-object --bucket ${aws_s3_bucket.jar_staging.bucket} --key ${aws_s3_object.object.key} ${aws_s3_object.object.key}
#   java -jar ${aws_s3_object.object.key}
#   EOF
#   tags = {
#     Name = "dennis-tf-web-server"
#   }
# }

# resource "aws_security_group" "allow_SSH" {
#   //b https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group
#   name        = "allow_SSH"
#   description = "Allow SSH inbound traffic"

#   ingress {
#     description      = "SSH from Anywhere"
#     from_port        = 22
#     to_port          = 22
#     protocol         = "tcp"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   ingress {
#     description      = "Spring Boot port from Anywhere"
#     from_port        = 8080
#     to_port          = 8080
#     protocol         = "tcp"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "allow_ssh"
#   }
# }

# resource "aws_iam_instance_profile" "s3_get_object_profile" {
#   // https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile#example-usage
#   name = "s3_get_object_profile"
#   role = aws_iam_role.ec2_assume.name
# }

# resource "aws_iam_role" "ec2_assume" {
#   // https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
#   name = "ec2_assume_dennis"

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Sid    = ""
#         Principal = {
#           Service = "ec2.amazonaws.com"
#         }
#       },
#     ]
#   })
# }

# resource "aws_iam_role_policy" "s3_get_object_policy" {
#   // https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy
#   name = "s3_get_object_policy"
#   role = aws_iam_role.ec2_assume.id

#   # Terraform's "jsonencode" function converts a
#   # Terraform expression result to valid JSON syntax.
#   policy = jsonencode({
#     "Version": "2012-10-17",
#     "Statement": [
#         {
#             "Effect": "Allow",
#             "Action": [
#                 "s3:ListBucket",
#                 "s3:GetObject"
#             ],
#             "Resource": [
#                 "${aws_s3_bucket.jar_staging.arn}",
#                 "${aws_s3_bucket.jar_staging.arn}/*"
#             ]
#         },
#         {
#             "Effect": "Allow",
#             "Action": "s3:ListAllMyBuckets",
#             "Resource": "*"
#         }
#     ]
# })
# }